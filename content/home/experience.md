+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Information Security Intern"
  company = "NASA - Kennedy Space Center"
  company_url = "https://nasa.gov/"
  location = "Cape Canaveral, Florida"
  date_start = "2019-06-03"
  date_end = "2019-08-09"
  description = """
  * Led and performed programmatic security compliance analysis and systems penetration testing in the Launch Control Center (LCC); Independently published technical report in the NASA Technical Reports Server (NTRS) on secure IT procurement and SCA.
  * Developed a novel invention, resulting in dramatic process improvements to SCA procedures, and formally presented the product to NASA superiors.
  * The only high school intern of the NASA/Kennedy Space Center 2019 Summer Session.
  """

[[experience]]
  title = "Data Analyst Intern"
  company = "Clean Footprint"
  company_url = "http://www.clean-footprint.com/"
  location = "Cape Canaveral, Florida"
  date_start = "2017-11-01"
  date_end = "2018-01-01"
  description = """
  * Developed a user-friendly Optical Character Recognition (OCR) scanning application in C# to automatically update information in an SQL database from paper documents, accelerating productivity of a previously labor intensive task.
  * https://github.com/xangelix/OCR2SQL
  """

[[experience]]
  title = "Infrastructure Planning and Laborer"
  company = "Sunrise Bath & Tile"
  company_url = "http://www.sunrisebathandtile.com/"
  location = "Cape Canaveral, Florida"
  date_start = "2017-06-01"
  date_end = "2017-08-01"
  description = """
  * Collaborated with mentors and employees to plan architectural layouts and install bath and tile related utilities and infrastructure.
  """

+++
